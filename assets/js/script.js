// custom navigation
const menu = document.querySelector('button.navbar-toggler');
const loginMenu = document.querySelector('.login-menu');
const subLoginMenu = document.querySelector('.sub-login-menu');
menu.addEventListener('click', function () {
  loginMenu.classList.toggle('d-none');
  subLoginMenu.classList.toggle('d-flex');
});

// custom owl carousel
$(document).ready(function () {
  $('.owl-carousel').owlCarousel();
});

var owl = $('.owl-carousel');
owl.owlCarousel({
  nav: true,
  dots: true,
  items: 6,
  loop: true,
  margin: 10,
  autoplay: true,
  autoplayTimeout: 1000,
  autoplayHoverPause: true,
  responsiveClass: true,
  responsive: {
    0: {
      items: 3,
      nav: true,
    },
    575: {
      items: 3,
      nav: false,
    },
    768: {
      items: 4,
      nav: true,
    },
    992: {
      items: 5,
      nav: true,
    },
    1200: {
      items: 6,
      nav: true,
    },
  },
});
